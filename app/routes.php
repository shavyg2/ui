<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::any('/',array('uses'=>'HomeController@index','as'=>'home'));

Route::any('/usability',array('uses'=>'HomeController@usability','as'=>'usability'));

Route::any('/contact',array('uses'=>'HomeController@contact','as'=>'contact'));

Route::any('/labs',array('uses'=>'HomeController@labs','as'=>'labs'));

Route::any('/assignments/1',array('uses'=>'HomeController@assignments1','as'=>'assignments'));

Route::any('/assignments/3',array('uses'=>'HomeController@assignments3','as'=>'assignments3'));

Route::any('/oath',array('uses'=>'HomeController@oath','as'=>'oath'));


Route::any("labs/2",array('uses'=>'LabController@two' ,'as'=>'lab2'));
Route::any("labs/3",array('uses'=>'LabController@three' ,'as'=>'lab3'));
Route::any("labs/4",array('uses'=>'LabController@four' ,'as'=>'lab4'));


Route::any("labs/5",array('uses'=>'LabController@five' ,'as'=>'lab5'));
Route::any("labs/8",array('uses'=>'LabController@eight' ,'as'=>'lab8'));


