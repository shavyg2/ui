<?php

class HomeController extends BaseController {


    public function index()
    {
        return View::make('pages.home');
    }

    public function usability(){
        return View::make('pages.usability');
    }

    public function contact(){
        return View::make('pages.contact');
    }

    public function labs(){
        return View::make('pages.lab.main');
    }

    public function assignments1(){
        return View::make('pages.assignment.main');
    }

    public function assignments3(){
        return Redirect::to("assignment3/game/index.html");
    }

    public function oath()
    {
        return View::make('pages.oath');
    }
}