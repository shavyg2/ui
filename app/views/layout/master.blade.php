<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>
        @if(isset($title))
        {{$title}}
        @else
        Shavauhn Gabay
        @endif
    </title>

    @include('header')
</head>
<body>
<div class="wrapper">
    @include('menu.nav')

    <div class="page">
        @yield('page')
    </div>

    @include('footer')
</div>


</body>
</html>